// VPC
resource "aws_vpc" "default" {
  cidr_block = "10.0.0.0/16"
  tags {
    Name = "rds-test"
  }
}

// Private subnets
resource "aws_subnet" "cache_a" {
  vpc_id = "${aws_vpc.default.id}"
  cidr_block = "10.0.1.0/24"
  availability_zone = "us-east-1a"
  tags {
    Name = "cache-a"
  }
}

resource "aws_subnet" "cache_b" {
  vpc_id = "${aws_vpc.default.id}"
  cidr_block = "10.0.2.0/24"
  availability_zone = "us-east-1b"
  tags {
    Name = "cache-b"
  }
}

// Security Group app
resource "aws_security_group" "app" {
  name = "rds-test-app"
  vpc_id = "${aws_vpc.default.id}"
  egress {
      from_port = 0
      to_port = 0
      protocol = "-1"
      cidr_blocks = ["0.0.0.0/0"]
  }
}

// Variables
variable "account" {}

// Parameter Group
resource "aws_db_parameter_group" "default" {
  name   = "test-rds-acme"
  family = "postgres9.5"

  parameter { name = "log_autovacuum_min_duration"     value = "0" }
  parameter { name = "log_hostname"                    value = "0" }
  parameter { name = "log_lock_waits"                  value = "1" }
  parameter { name = "log_min_duration_statement"      value = "1000" }
  parameter { name = "log_temp_files"                  value = "0" }

  parameter { name = "track_io_timing"                 value = "1" }
  parameter { name = "track_functions"                 value = "all" }

  parameter { name = "autovacuum_analyze_scale_factor" value = "0.02"              apply_method = "pending-reboot" }
  parameter { name = "autovacuum_max_workers"          value = "3"                 apply_method = "pending-reboot" }
  parameter { name = "autovacuum_vacuum_scale_factor"  value = "0.05"              apply_method = "pending-reboot" }

  parameter { name = "timezone"                        value = "America/Sao_Paulo" apply_method = "pending-reboot" }
}

// RDS with HA
module "test" {
  source  = "../"
  account = "${var.account}"

  name = "test-acme-rds"
  subnet_ids = [
    "${aws_subnet.cache_a.id}",
    "${aws_subnet.cache_b.id}"
  ]
  vpc_id = "${aws_vpc.default.id}"
  tags = {
    Foo = "bar"
  }
  password = "123mudar"
  username = "administrator"
  multi_az = false
  skip_final_snapshot = true
  auto_minor_version_upgrade = true
  copy_tags_to_snapshot      = true
  // Per engine
  parameter_group_name = "${aws_db_parameter_group.default.id}"
  port                 = 5432
  engine               = "postgres"
  engine_version       = "9.5.4"
  family               = "postgres9.5"
}

output "test" {
  value = "${module.test.db_instance}"
}
