// DB Instance
resource "aws_db_instance" "rds" {
  allocated_storage = "${var.allocated_storage}"
  engine            = "${var.engine}"
  engine_version    = "${var.engine_version}"
  identifier        = "${var.name}"
  instance_class    = "${var.instance_class}"
  storage_type      = "${var.storage_type}"
  copy_tags_to_snapshot = "${var.copy_tags_to_snapshot}"
  final_snapshot_identifier = "snapshot-${var.name}-final"
  skip_final_snapshot       = "${var.skip_final_snapshot}"
  name     = "${var.db_name}"
  password = "${var.password}"
  username = "${var.username}"

  backup_retention_period = "${var.backup_retention_period}"
  backup_window           = "${var.backup_window}"
  iops                    = "${var.iops}"
  maintenance_window      = "${var.maintenance_window}"
  auto_minor_version_upgrade = "${var.auto_minor_version_upgrade}"

  multi_az               = "${var.multi_az}"
  port                   = "${var.port}"
  publicly_accessible    = false
  vpc_security_group_ids = ["${compact(concat(
    list(aws_security_group.rds.id),
    var.extra_security_groups
  ))}"]
  db_subnet_group_name   = "${aws_db_subnet_group.default.id}"
  parameter_group_name   = "${var.parameter_group_name}"
  storage_encrypted      = "${var.storage_encrypted}"
  apply_immediately      = "${var.apply_immediately}"

  monitoring_role_arn = "${var.monitoring_role_arn}"
  monitoring_interval = "${var.monitoring_interval}"

  deletion_protection = "${var.deletion_protection}"
  enabled_cloudwatch_logs_exports = "${var.enabled_cloudwatch_logs_exports}"

  kms_key_id         = "${var.kms_key_id}"
  tags = "${var.tags}"
}

output "db_instance" {
  value = {
    id             = "${aws_db_instance.rds.id}"
    address        = "${aws_db_instance.rds.address}"
    arn            = "${aws_db_instance.rds.arn}"
    endpoint       = "${aws_db_instance.rds.endpoint}"
    engine         = "${aws_db_instance.rds.engine}"
    engine_version = "${aws_db_instance.rds.engine_version}"
    instance_class = "${aws_db_instance.rds.instance_class}"
    name           = "${aws_db_instance.rds.name}"
    port           = "${aws_db_instance.rds.port}"
    status         = "${aws_db_instance.rds.status}"
    username       = "${aws_db_instance.rds.username}"
    hosted_zone_id = "${aws_db_instance.rds.hosted_zone_id}"
  }
}
