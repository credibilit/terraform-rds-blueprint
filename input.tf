variable "account" {
}

variable "name" {
  description = "The name of the RDS instance and some components"
}

variable "subnet_ids" {
  type = "list"
  description = "A list of VPC subnet IDs"
}

variable "tags" {
  type = "map"
  default = {}
}

variable "vpc_id" {
  description = "The VPC ID"
}

variable "port" {
  description = "List port"
}

variable "family" {
  description = "The family of the DB"
}

variable "allocated_storage" {
  description = "The allocated storage in gigabytes"
  default = 100
}

variable "engine" {
  description = "The database engine to use"
}

variable "engine_version" {
  description = "The engine version to use"
}

variable "instance_class" {
  description = "The instance type of the RDS instance"
  default = "db.m3.medium"
}

variable "storage_type" {
  description = "Storage type: standard, gp2, io1"
  default = "gp2"
}

variable "skip_final_snapshot" {
  description = ""
  default = false
}

variable "db_name" {
  description = "The DB name to create"
  default = ""
}

variable "password" {
  description = "Password for the master DB user"
}

variable "username" {
  description = "Username for the master DB user"
}

variable "backup_retention_period" {
  description = "The days to retain backups for. Must be 1 or greater to be a source for a Read Replica"
  default = 7
}

variable "backup_window" {
  description = "The backup window"
  default = "04:00-06:00"
}

variable "iops" {
  description = "The amount of provisioned IOPS."
  default = 0
}

variable "maintenance_window" {
  description = "The window to perform maintenance in"
  default = "sun:06:00-sun:08:00"
}

variable "multi_az" {
  description = "Specifies if the RDS instance is multi-AZ"
  default = true
}

variable "storage_encrypted" {
  description = "Specifies whether the DB instance is encrypted"
  default = false
}

variable "apply_immediately" {
  description = "Specifies whether any database modifications are applied immediately, or during the next maintenance window"
  default = true
}

variable "monitoring_role_arn" {
  description = "The ARN for the IAM role that permits RDS to send enhanced monitoring metrics to CloudWatch Logs"
  default = ""
}

variable "auto_minor_version_upgrade" {
  description = "Indicates that minor engine upgrades will be applied automatically to the DB instance during the maintenance window"
  default     = false
}

variable "monitoring_interval" {
  description = "The interval, in seconds, between points when Enhanced Monitoring metrics are collected for the DB instance"
  default = 0
}

variable "kms_key_id" {
  description = "The ARN for the KMS encryption key"
  default = ""
}

variable "parameter_group_name" {
  description = "Name of the DB parameter group to associate"
}

variable "extra_security_groups" {
  description = "Extra Security Groups"
  type = "list"
  default = []
}

variable "copy_tags_to_snapshot" {
  description = "On delete, copy all Instance tags to the final snapshot (if final_snapshot_identifier is specified). Default is false."
  default     = false
}

variable "deletion_protection" {
  description = "If the DB instance should have deletion protection enabled. The database can't be deleted when this value is set to true. The default is false."
  default = false
}

variable "enabled_cloudwatch_logs_exports" {
  description = "List of log types to enable for exporting to CloudWatch logs. If omitted, no logs will be exported. Valid values (depending on engine): alert, audit, error, general, listener, slowquery, trace."
  type = "list"
  default = []
}