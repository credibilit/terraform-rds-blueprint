// Subnet Group
resource "aws_db_subnet_group" "default" {
  name        = "${var.name}"
  description = "${title(var.name)} Subnet Group"
  subnet_ids  = ["${var.subnet_ids}"]
  tags        = "${var.tags}"
}

output "db_subnet_group" {
  value = {
    id  = "${aws_db_subnet_group.default.id}"
    arn = "${aws_db_subnet_group.default.arn}"
  }
}
