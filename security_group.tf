// Security Group
resource "aws_security_group" "rds" {
  name   = "rds-${var.name}"
  vpc_id = "${var.vpc_id}"

  tags {
    Name = "rds-${var.name}"
  }
}

resource "aws_security_group_rule" "rds_egress" {
  security_group_id = "${aws_security_group.rds.id}"
  type = "egress"
  from_port = 0
  to_port = 0
  protocol = "-1"
  cidr_blocks = ["0.0.0.0/0"]
}

output "security_group" {
  value = {
    id = "${aws_security_group.rds.id}"
  }
}
